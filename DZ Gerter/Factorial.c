#include <stdio.h>

int main()
{
	int odds[10];
	for (int i = 0; i < 10; i++)
	{
		odds[i] = i;
		for (int n = 1; n < i; n++) 
		{
			odds[i] = odds[i] * (i - n);
		}
		if (i == 9) printf("%d. ", odds[i]);
		else printf("%d, ", odds[i]);
	}
	getch();
	return 0;
}